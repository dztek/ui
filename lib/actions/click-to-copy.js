export function clickToCopy(node, passedInValue) {

	function selectAndCopy(text) {
		let tmpEl = document.createElement('textarea');
		tmpEl.innerText = text;
		tmpEl.focus();
		tmpEl.select();
		document.execCommand('copy');
		tmpEl = null;
	}

	async function handler (ev) {
		const text = ev.target.value || ev.target.innerText || ev.target.innerHTML;
		// select and copy to cb
		selectAndCopy(text);
		// or just copy directly to cb
		await navigator?.clipboard?.writeText(text);
	}

	node.onclick = passedInValue ? () => { handler({ target: { value: passedInValue }}) } : handler;

	return {
		update(updatedNode) {
			if (updatedNode) {
				node.onclick = () => handler({ target: updatedNode })
			}
		},
		destroy() {
			node.removeEventListener('click', handler);
		}
	}
}
