# @dztek/ui
UI components, actions, and services for app assembling

## Svelte Actions
### clickToCopy
Click any HTML element to copy its contents. Works on regular elements as well as input elements.

#### Regular element example:
This will copy the text within the element with the clickToCopy action after it has been clicked
```html
<script>
  import { clickToCopy } from "@dztek/ui/lib/actions/click-to-copy";
</script>

<div use:clickToCopy>
  Something to copy
</div>
```
#### Button example:
This will copy the text inside the bound target elment to clipboard when the element with the clickToCopy action is clicked
```html
<script>
  import { clickToCopy } from "@dztek/ui/lib/actions/click-to-copy";

  let el;
</script>

<div bind:this={el}>
  To be copied on button press!
</div>

<button use:clickToCopy={el}>
  Copy to clipboard
</button>
```

#### Text example:
This will copy provided text to clipboard when the element with the clickToCopy action is clicked
```html
<script>
  import { clickToCopy } from "@dztek/ui/lib/actions/click-to-copy";

  const text = "Some text to copy";
</script>

<button use:clickToCopy={text}>
  Copy to clipboard
</button>
```